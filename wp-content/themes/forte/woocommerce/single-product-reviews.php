<?php
/**
 * Display single product reviews (comments)
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */
global $woocommerce; ?>
<?php if ( comments_open() ) : ?><div id="reviews"><?php 
	
	echo '<div id="comments">';
	
	$count = $wpdb->get_var("
		SELECT COUNT(meta_value) FROM $wpdb->commentmeta 
		LEFT JOIN $wpdb->comments ON $wpdb->commentmeta.comment_id = $wpdb->comments.comment_ID
		WHERE meta_key = 'rating'
		AND comment_post_ID = $post->ID
		AND comment_approved = '1'
		AND meta_value > 0
	");
	
	$rating = $wpdb->get_var("
		SELECT SUM(meta_value) FROM $wpdb->commentmeta 
		LEFT JOIN $wpdb->comments ON $wpdb->commentmeta.comment_id = $wpdb->comments.comment_ID
		WHERE meta_key = 'rating'
		AND comment_post_ID = $post->ID
		AND comment_approved = '1'
	");
	
	if ( $count > 0 ) :
		
		$average = number_format($rating / $count, 2);
		
		echo '<div itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">';
		
		echo '<div class="star-rating" title="'.sprintf(__('Rated %s out of 5', 'forte'), $average).'"><span style="width:'.($average*16).'px"><span itemprop="ratingValue" class="rating">'.$average.'</span> '.__('out of 5', 'forte').'</span></div>';

		echo '<div class="clear"></div>';
		
		echo '<h4>'.sprintf( _n('%s review for %s', '%s reviews for %s', $count, 'forte'), '<span itemprop="ratingCount" class="count">'.$count.'</span>', wptexturize($post->post_title) ).'</h4>';

		echo '</div>';
		
	else :
	
		echo '<h4>'.__('Reviews', 'forte').'</h4>';
		
	endif;

	$title_reply = '';

	if ( have_comments() ) : 

		echo '<ol class="commentlist">';
		
		wp_list_comments( array( 'callback' => 'woocommerce_comments' ) );

		echo '</ol>';
	
		if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : ?>
			<div class="navigation">
				<div class="alignleft"><?php previous_comments_link( __( '<div class="nav-previous pix_button tiny_button"><span class="meta-nav">&larr;</span> Previous</div>', 'forte' ) ); ?></div>
				<div class="alignright"><?php next_comments_link( __( '<div class="nav-next pix_button tiny_button">Next <span class="meta-nav">&rarr;</span></div>', 'forte' ) ); ?></div>
			</div>

			<div class="clear"></div>
		<?php endif;
						
	else : 
		
		echo '<p>'.__('There are no reviews yet, would you like to <a href="#review_form" class="inline show_review_form">submit yours</a>?', 'forte').'</p>';
	
	endif;
	
	$commenter = wp_get_current_commenter();
	
	echo '</div><div id="review_form_wrapper"><div id="review_form">';
	
	$comment_form = array(
		'title_reply' => '',
		'comment_notes_before' => '',
		'comment_notes_after' => '',
		'fields' => array(
			'author' => '<p class="comment-form-author">' . '<label for="author">' . __( 'Name', 'forte' ) . '</label> ' . '<span class="required">*</span>:<br>' .
			            '<input id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30" aria-required="true" /></p>',
			'email'  => '<p class="comment-form-email"><label for="email">' . __( 'Email', 'forte' ) . '</label> ' . '<span class="required">*</span>:<br>' .
			            '<input id="email" name="email" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) . '" size="30" aria-required="true" /></p>',
		),
		'label_submit' => __('Submit Review', 'forte'),
		'logged_in_as' => '',
		'comment_field' => ''
	);
		
	if ( get_option('woocommerce_enable_review_rating') == 'yes' ) {
	
		$comment_form['comment_field'] = '<p class="comment-form-rating"><label for="rating">' . __('Rating', 'forte') .':</label><select name="rating" class="letmebe" id="rating">
			<option value="">'.__('Rate...', 'forte').'</option>
			<option value="5">'.__('Perfect', 'forte').'</option>
			<option value="4">'.__('Good', 'forte').'</option>
			<option value="3">'.__('Average', 'forte').'</option>
			<option value="2">'.__('Not that bad', 'forte').'</option>
			<option value="1">'.__('Very Poor', 'forte').'</option>
		</select></p>';
			
	}
	
	$comment_form['comment_field'] .= '<p class="comment-form-comment"><label for="comment">' . __( 'Your Review', 'forte' ) . ':</label><textarea id="comment" name="comment" cols="45" rows="8" aria-required="true"></textarea></p>' . $woocommerce->nonce_field('comment_rating', true, false);
	
	comment_form( $comment_form ); 

	echo '</div></div>';
	
?><div class="clear"></div></div>
<?php endif; ?>